from receipts.views import ReceiptListView
from django.urls import path


urlpatterns = [path("", ReceiptListView.as_view(), name="home")]
