from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import TemplateView


# Create your views here.
class ReceiptListView(LoginRequiredMixin, TemplateView):
    template_name = "receipts/list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context
