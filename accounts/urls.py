from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView
from accounts.views import sign_up_form

# from django.contrib.auth import CreateView


urlpatterns = [
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    # path("", ExpensesListView.as_view, name="expenses"),
    path("signup/", sign_up_form, name="signup"),
]
